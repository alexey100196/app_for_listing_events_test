import React from 'react';
import './styles/index.scss';

import {Header} from './components/header/';
import {EventList} from './components/eventList';

function App() {
  return (
    <div className='App'>
      <div className='container'>
          <Header/>
          <EventList/>
      </div>
    </div>
  )
}

export default App;
