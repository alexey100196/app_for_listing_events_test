import React from "react";
import { ReactComponent as CheckSvg } from '../../assets/images/check.svg';

export const EventListItem = ({item}) => {
  const {type, value, receivedAt} = item;

  return (
    <div className='event-list__item'>
      <div className='event-list__item-mark'>
        <CheckSvg/>
      </div>
      <div className='event-list__type'>
        {type}
      </div>
      <div className='event-list__item-value'>
        {value}
      </div>
      <div className='event-list__item-date'>
        {receivedAt}
      </div>
    </div>
  );
}
