import React from "react";

import { EventListItem } from "./EventListItem";
import useEventService from '../../hooks/useEventService';


export const EventList = () => {
  const { filteredEvents } = useEventService();

  return filteredEvents.map(item => {
      return (
        <EventListItem item={item} key={item.messageId}/>
      )
    }
  )
}
