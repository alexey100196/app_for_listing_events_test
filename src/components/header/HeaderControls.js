import React from 'react'
import { useSelector, useDispatch } from "react-redux";
import { eventActions } from '../../redux/actions/events';

export const HeaderControls = () => {
  const dispatch = useDispatch();
  const {isFetching} = useSelector(state => state.events);

  const OnFetching = () => {
    dispatch(eventActions.turnFetchOn());
  }

  const StopFetching = () => {
    dispatch(eventActions.turnFetchOff());
  }

  return (
    <div className='header__btns'>
      <button
        className={`btn btn--primary ${isFetching ? "btn--active" : ""}`}
        onClick={OnFetching}>live</button>
      <button
        className={`btn btn--primary ${!isFetching ? "btn--active" : ""}`}
        onClick={StopFetching}>pause</button>
    </div>
  )
}

