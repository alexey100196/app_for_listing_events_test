import React, {useEffect} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {eventActions} from "../../redux/actions/events";

export const HeaderSearchBox = () => {
  const dispatch = useDispatch();
  const {filterValue} = useSelector(state => state.events);

  const onInputChange = ({target: {value: newValue}}) => dispatch(eventActions.setFilterValue(newValue));

  const [inputValue] = React.useState('');

  useEffect(() => {
    dispatch(eventActions.setFilterValue(inputValue));
  }, [inputValue]);

  return (
    <div className='header__search-box'>
      <input className='header__search-input'
             type='text'
             placeholder='Type to search'
             value={filterValue}
             onChange={onInputChange}
      />
    </div>
  )
}

