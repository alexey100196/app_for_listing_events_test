import React from 'react';

import { HeaderControls } from './HeaderControls';
import { HeaderSearchBox } from './HeaderSearchBox';

export const Header = () => {
  return (
    <div className='header'>
      <HeaderControls/>
      <HeaderSearchBox />
    </div>
  )
}
