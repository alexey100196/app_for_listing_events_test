import {useEffect} from "react";
import { socket } from '../core'
import { useSelector, useDispatch } from "react-redux";
import { eventActions } from "../redux/actions/events";

const useEventService = () => {
  const {list, isFetching, filterValue}= useSelector(state => state.events);
  const filteredEvents = filterValue ? list.filter(event => event.value.toLowerCase().startsWith(filterValue.toLowerCase())) : list
  const dispatch = useDispatch();

  useEffect(() => {

    if (!isFetching) {
      socket.off('message');
      socket.off('error');
      return;
    };

    socket.on('error', error => {
      console.log(error);
    });

    socket.on('message', data => {
      dispatch(eventActions.fillEvents(data));
    });
  }, [isFetching]);
  return {filteredEvents};
}
export default useEventService;
