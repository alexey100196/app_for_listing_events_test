// Core
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

// Components
import App from "./App";

// Instruments
import { store } from './init/store';

render(
  <Provider store = { store }>
    <App />
  </Provider>,
  document.getElementById('root')
);
