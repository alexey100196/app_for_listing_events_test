// Core
import { combineReducers } from 'redux';

// Reducers
import { eventsReducer as events } from '../redux/reducers/events';

export const rootReducer = combineReducers({
  events
});
