import { types } from '../types';

export const eventActions = {
  fillEvents: (payload) => {
    return {
      type: types.FILL_EVENTS,
      payload
    }
  },

  turnFetchOff: () => {
    return {
      type: types.TURN_FETCH_OFF
    }
  },

  turnFetchOn: () => {
    return {
      type: types.TURN_FETCH_ON
    }
  },

  setFilterValue: (payload) => {
    return {
      type: types.SET_FILTER_VALUE,
      payload
    }
  },
};
