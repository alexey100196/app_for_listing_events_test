import {types} from '../types';

const initialState = {
  list: [],
  isFetching: false,
  filterValue: ''
};

export const eventsReducer = (state = initialState, action) => {
  const {payload} = action;

  switch (action.type) {
    case types.FILL_EVENTS:
      return {
        ...state,
        list: [...state.list, payload].slice(0, 99)
      };

    case types.TURN_FETCH_OFF:
      return {
        ...state,
        isFetching: false
      };

    case types.TURN_FETCH_ON:
      return {
        ...state,
        isFetching: true
      };

    case types.SET_FILTER_VALUE:
      return {
        ...state,
        filterValue: payload
      };

    default:
      return state;
  }
};
